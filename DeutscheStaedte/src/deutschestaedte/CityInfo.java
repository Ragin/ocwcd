/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package deutschestaedte;

   /**
    * 
    * @author Connacht
    */
    class CityInfo {

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }


        private Integer id; 
        private String name; 
        private String einwohner; 
        private String land; 

        /**
         * @return the name
         */
        public String getName() {
            return name;
        }

        /**
         * @param name the name to set
         */
        public void setName(String name) {
            this.name = name;
        }

        /**
         * @return the einwohner
         */
        public String getEinwohner() {
            return einwohner;
        }

        /**
         * @param einwohner the einwohner to set
         */
        public void setEinwohner(String einwohner) {
            this.einwohner = einwohner;
        }

        /**
         * @return the land
         */
        public String getLand() {
            return land;
        }

        /**
         * @param land the land to set
         */
        public void setLand(String land) {
            this.land = land;
        }
        
    }