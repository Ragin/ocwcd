package deutschestaedte;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.Statement;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.ragin.URLReader;
import us.codecraft.xsoup.Xsoup;

import org.pmw.tinylog.Logger;
import org.ragin.PropertyReader;

/**
 * A little bit overload: - additional to the array holding all values and used
 * to write CSV and database we create an array of Objects type CityInfo. This
 * is not used anywhere later.
 */
public class DeutscheStaedte {

    private static final String WIKIPEDIA_URL = "https://de.wikipedia.org/wiki/Liste_der_Gro%C3%9Fst%C3%A4dte_in_Deutschland";
    private static final String DEUTSCHESTAEDTE_RAW = "staedte.txt";
    private static final String DEUTSCHESTAEDTE_DB = "deutsche-staedte.db";
    private static final String DEUTSCHESTAEDTE_CSV = "deutsche-staedte.csv";

    /**
     * @param args the command line arguments
     * @throws java.io.IOException
     */
    public static void main(String[] args) throws IOException {
        Logger.info("Application started");
        //doPlainOldJava();
        DeutscheStaedte ds = new DeutscheStaedte();
        ds.doJsoupRequest();
        Logger.info("Application stopped.");
    }

    /**
     *
     * @throws IOException
     */
    public String doJsoupRequest() throws IOException {
        Logger.info("Entered doJsoupRequest()");
        CityInfo tmpCity;
        String strReturn = "";
        // using jsoup
        Document document = Jsoup.connect(WIKIPEDIA_URL).get();
        // Element content = document.getElementById("content");

        // name: //*[@id="mw-content-text"]/table[1]/tbody/tr/td[2]/table/tbody/tr[5]/td[1]/a
        //List<String> listCities = Xsoup.compile("/html/body/div[3]/div[3]/div[4]/table[1]/tbody/tr/td[2]/table/tbody/tr[@style=height:51px]/td[1]/a/text()").evaluate(document).list();
        List<String> listCities = Xsoup.compile("/html/body/div[3]/div[3]/div[4]/table[1]/tbody/tr/td[2]/table/tbody/tr[@style=height:51px]/td[1]").evaluate(document).list();
        int groesse = listCities.size();

        strReturn += "Found " + groesse + " rows.";
        Logger.info(strReturn);
        String[][] arrCities = new String[groesse][3];
        Map<Integer, CityInfo> hashMapCityInfo = new HashMap<Integer, CityInfo>();

        int count = 0;
        Logger.info("Found " + listCities.size() + " cities elements.");
        /*
        problem is that most elements are
        <td align="left"><a href="/wiki/Siegen" title="Siegen">Siegen</a><span class="reference"><sup id="FN_1_back"><a href="#FN_1">1</a></sup></span></td>
        while 2 are
        <td align="left"><span style="white-space:nowrap"><a href="/wiki/M%C3%B6nchengladbach" title="Mönchengladbach">Mönchengladbach</a><span class="reference"><sup id="FN_1_back"><a href="#FN_1">1</a></sup></span><sup>,</sup> <span class="reference"><sup id="FN_5_back"><a href="#FN_5">5</a></sup></span></span></td>
        so we extract the city name using regex
         */
        for (String strCurrentCity : listCities) {
            count++;
            //System.out.println( strCurrentCity);  

            //  extract all characters btween title="*"> and the </a></span tags
            Pattern p = Pattern.compile("title=.*\">(.*?)");

            Matcher m = p.matcher(strCurrentCity);
            String result = null;
            String strCity = null;
            while (m.find()) {
                result = m.group(0);//this is the city name: group 1
                //System.out.println( result);
                int firstHochkomma, secondHochkomma;
                firstHochkomma = result.indexOf("\"");
                secondHochkomma = result.indexOf("\"", result.indexOf("\"") + 1);
                strCity = result.substring(firstHochkomma + 1, secondHochkomma);
                //System.out.println("Second index of hochkomma is: " + secondHochkomma);
                //System.out.println("Stadt: " + strCity);
            }

            CityInfo currentCity = new CityInfo();
            currentCity.setId((Integer) count); // set count
            currentCity.setName(strCity); // set name
            hashMapCityInfo.put((Integer) count, currentCity);
            arrCities[count - 1][0] = strCity;
        }

        // finding the inhabitants is quite easy as here we have no xpath differences
        count = 0;
        List<String> listEinwohner = Xsoup.compile("/html/body/div[3]/div[3]/div[4]/table[1]/tbody/tr/td[2]/table/tbody/tr[@style=height:51px]/td[10]/span/text()").evaluate(document).list();
        System.out.println("Found " + listEinwohner.size() + " inhabitant elements.");
        Logger.info("Found " + listEinwohner.size() + " inhabitant elements.");
        for (String strCurrentEinwohner : listEinwohner) {
            count++;
            String fineEinwohner = strCurrentEinwohner.trim().replaceAll(".0000000000", "");
            fineEinwohner = Integer.valueOf(fineEinwohner).toString(); // remove leading 0
            arrCities[count - 1][1] = fineEinwohner.trim();
            tmpCity = hashMapCityInfo.get(count);
            tmpCity.setEinwohner(fineEinwohner);
            hashMapCityInfo.put(count, tmpCity);  // write back
            //System.out.println(fineEinwohner);
        }

        // finding the lands is quite easy as here we have no xpath differences
        count = 0;
        List<String> listLand = Xsoup.compile("/html/body/div[3]/div[3]/div[4]/table[1]/tbody/tr/td[2]/table/tbody/tr[@style=height:51px]/td[15]/span/text()").evaluate(document).list();
        Logger.info("Found " + listLand.size() + " land elements.");
        System.out.println("Found " + listLand.size() + " land elements.");
        for (String strCurrentLand : listLand) {
            count++;
            //System.out.println( strCurrentCity);  
            arrCities[count - 1][2] = strCurrentLand.trim();
            //System.out.println(strCurrentLand);
            tmpCity = hashMapCityInfo.get(count);
            tmpCity.setLand(strCurrentLand);
            hashMapCityInfo.put(count, tmpCity);  // write back
        }

        Logger.info("HashMap size: " + hashMapCityInfo.size());
        System.out.println("");
        writeCSV(arrCities, DEUTSCHESTAEDTE_CSV);
        strReturn += "Written: " + DEUTSCHESTAEDTE_CSV + "\n";
        try {
            writeDatabase(arrCities, DEUTSCHESTAEDTE_DB);
        } catch (Exception ex) {
            Logger.error("Entered doJsoupRequest()");
            System.out.println("sqlite file seems to exist!");
        }
        strReturn += "Written: " + DEUTSCHESTAEDTE_DB + "\n";

        /*
        Elements links = content.getElementsByTag("a");
        for (Element link : links) {
            String linkHref = link.attr("href");
            String linkText = link.text();
            System.out.println("Text::"+linkText+", URL::"+linkHref);
        }
         */
        return strReturn;
    }

    /**
     *
     * @param arrCities
     * @param fileNameCSV
     */
    private void writeCSV(String[][] arrCities, String fileNameCSV) {
        Logger.info("Entered writeCSV()");
        fileNameCSV = "output/" + fileNameCSV;
        File f = new File(fileNameCSV);
        if (f.exists()) {
            f.delete();
            //Random random = new Random();
            //fileNameSqlite = random.nextInt(1000) + 1 + "_" + fileNameSqlite;
            System.out.println("Deleted old version of: " + fileNameCSV);
            Logger.info("Deleted old version of: " + fileNameCSV);

        }
        StringBuilder sb = new StringBuilder();
        try {
            PrintWriter pw = new PrintWriter(fileNameCSV);

            for (String[] stadt : arrCities) {
                //System.out.println(stadt[0] + " - " + stadt[2] + " : " + stadt[1]);
                sb.append(stadt[0]).append(",").append(stadt[2]).append(",").append(stadt[1]).append(System.getProperty("line.separator"));
            }
            pw.write(sb.toString());
            pw.close();
        } catch (IOException iOException) {
            iOException.printStackTrace();
        } finally {
            Logger.info("Written " + arrCities.length + " cities to CSV: " + fileNameCSV);
            System.out.println("Written " + arrCities.length + " cities to CSV: " + fileNameCSV);
        }

    }

    /**
     *
     * @param arrCities
     * @param fileNameSqlite
     * @throws Exception
     */
    private void writeDatabase(String[][] arrCities, String fileNameSqlite) throws Exception {
        Logger.info("Entered writeDatabase()");
        PropertyReader pr = new PropertyReader();

        fileNameSqlite = "output/" + fileNameSqlite;
        File f = new File(fileNameSqlite);
        if (f.exists()) {
            boolean deleteSuccess = f.delete();
            if (deleteSuccess == true) {
                Logger.info("Deleted old version of: " + fileNameSqlite);
                System.out.println("Deleted old version of: " + fileNameSqlite);
            } else {
                Logger.info("Old version of: " + fileNameSqlite + " could NOT be deleted.");
            }

            //Random random = new Random();
            //fileNameSqlite = random.nextInt(1000) + 1 + "_" + fileNameSqlite;
        }
        Connection c = null;
        Statement stmt = null;
        try {
            /*
            Class.forName("org.sqlite.JDBC");
            c = DriverManager.getConnection("jdbc:sqlite:" + fileNameSqlite);
             */
            c = pr.getConnection();
            System.out.println("Opened database successfully: " + fileNameSqlite);
            Logger.debug("Opened database successfully: " + fileNameSqlite);
            stmt = c.createStatement();
            String sql = "CREATE TABLE CITIES ( "
                    + " NAME CHAR(50), "
                    + " LAND CHAR(50), "
                    + " EINWOHNER CHAR(50)) ";
            stmt.executeUpdate(sql);
            stmt.close();
            c.close();
        } catch (Exception e) {
            System.err.println(e.getClass().getName() + ": " + e.getMessage());
            Logger.error(e.getClass().getName() + ": " + e.getMessage());
            System.exit(0);
        }
        System.out.println("Table created successfully");
        Logger.info("Table created successfully");
        // now inserting
        try {
            Class.forName("org.sqlite.JDBC");
            c = DriverManager.getConnection("jdbc:sqlite:" + fileNameSqlite);
            c.setAutoCommit(false);
            System.out.println("Opened database successfully : " + fileNameSqlite);
            Logger.debug("Opened database successfully : " + fileNameSqlite);
            for (String[] stadt : arrCities) {
                stmt = c.createStatement();
                String sql = "INSERT INTO CITIES (NAME,LAND,EINWOHNER) "
                        + "VALUES ( '" + stadt[0] + "', '" + stadt[2] + "', '" + stadt[1] + "');";
                stmt.executeUpdate(sql);
            }
            stmt.close();
            c.commit();
            c.close();
        } catch (Exception e) {
            System.err.println(e.getClass().getName() + ": " + e.getMessage());
            Logger.error(e.getClass().getName() + ": " + e.getMessage());
            System.exit(0);
        }
        Logger.info("Written " + arrCities.length + " cities to SQLITE database: " + fileNameSqlite);
    }

    /**
     *
     * @param fileNameRAW
     */
    public void doPlainOldJavaRequest(String fileNameRAW) {
        Logger.info("Entered doPlainOldJavaRequest()");
        fileNameRAW = "output/" + fileNameRAW;
        URLReader urlReader = new URLReader();
        String rawHtml = urlReader.readURL(WIKIPEDIA_URL, true);
        // System.out.println(rawHtml);
        BufferedWriter out = null;
        try {
            out = new BufferedWriter(new FileWriter(DEUTSCHESTAEDTE_RAW, true));
            out.write(rawHtml);
            out.close();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            Logger.info("Written response to RAW file: " + fileNameRAW);
        }
    }
}
