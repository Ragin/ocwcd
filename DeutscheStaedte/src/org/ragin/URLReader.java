/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.ragin;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;

/**
 * read website without
 * @author Connacht
 * 
 */
public class URLReader {

    /**
     *
     * @param address
     * @param withNewline
     * @return
     */
    public String readURL(String address , boolean withNewline) {
        if (!(netIsAvailable())) return "Internet not available "; 
        StringBuilder buff = new StringBuilder();
        try {
            URL url = new URL(address); 
            BufferedReader br = new BufferedReader(new InputStreamReader(url.openStream()));
            String line =null; 
            
            while((line = br.readLine()) != null) {  
                buff.append(line); 
                if (withNewline == true) buff.append(line + "\n");
                // skip crlf
            }
            br.close();
            
       } catch (MalformedURLException e) {
      } catch (IOException e) {
      }
        return buff.toString(); 
    }
    
    private static boolean netIsAvailable() {                                                                                                                                                                                                 
    try {                                                                                                                                                                                                                                 
        final URL url = new URL("http://www.google.com");                                                                                                                                                                                 
        final URLConnection conn = url.openConnection();                                                                                                                                                                                  
        conn.connect();                                                                                                                                                                                                                   
        return true;                                                                                                                                                                                                                      
    } catch (MalformedURLException e) {                                                                                                                                                                                                   
        throw new RuntimeException(e);                                                                                                                                                                                                    
    } catch (IOException e) {                                                                                                                                                                                                             
        return false;                                                                                                                                                                                                                     
    }                                                                                                                                                                                                                                     
}  
}
